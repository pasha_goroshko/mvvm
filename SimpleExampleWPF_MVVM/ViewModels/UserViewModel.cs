﻿using SimpleExampleWPF_MVVM.Models;
namespace SimpleExampleWPF_MVVM.ViewModels
{
    using System.Collections.ObjectModel;

    public class UserViewModel
    {
        public ObservableCollection<User> Users { get; set; }
        public UserViewModel()
        {
            Init();
        }

        private void Init()
        {
            Users = new ObservableCollection<User>
            {
                new User { Id = 1, Login = "#1_user", Name = "Name_*1" },
                new User { Id = 2, Login = "#2_user", Name = "Name_*2" },
                new User { Id = 3, Login = "#3_user", Name = "Name_*3" },
                new User { Id = 4, Login = "#4_user", Name = "Name_*4" },
                new User { Id = 5, Login = "#5_user", Name = "Name_*5" },
                new User { Id = 6, Login = "#6_user", Name = "Name_*6" },
                new User { Id = 7, Login = "#7_user", Name = "Name_*7" },
                new User { Id = 8, Login = "#8_user", Name = "Name_*8" },
                new User { Id = 9, Login = "#9_user", Name = "Name_*9" },
                new User { Id = 10, Login = "#10_user", Name = "Name_*10" }
            };
        }

    }
}
